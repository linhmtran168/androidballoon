package com.androidgame.balloon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.androidgame.framework.Pixmap;
import com.androidgame.framework.ngin.Pool;
import com.androidgame.framework.ngin.Pool.PoolObjectFactory;

public class Stage {
	static final float TICK = 0.04f;
	static final int BALLOON_WIDTH = 46;
	static final int BALLOON_HEIGHT = 54;
	// Create the pool to hold and reuse balloon instance
	Pool<Balloon> balloonPool;
	// The list of balloons that are currently on the screen
	List<Balloon> balloonList = new ArrayList<Balloon>();
	float tickTime = 0;
	boolean gameOver = false;
	Random random = new Random();
	public static Pixmap image;
	public static int image_x;
	public static int image_y;
	int timeLeft = 1200;
	int createInterval = 0;
	int score = 0;
	
	public Stage() {
		PoolObjectFactory<Balloon> factory = new PoolObjectFactory<Balloon>() {
			public Balloon createObject() {
				return new Balloon();
			}
		};
		
		balloonPool = new Pool<Balloon>(factory, 50);
	}
	
	
	public void update(float deltaTime, float timeStart) {
		if (gameOver) {
			return;
		}
		int timePast = (int)(System.nanoTime() - timeStart)/1000000000;
		timeLeft = timeLeft - timePast;
		if (timeLeft <= 0) {
			gameOver = true;
		}
		tickTime += deltaTime;
		// After TICK seconds update the state of the balloons
		while (tickTime > TICK) {
			tickTime -= TICK;
			// Load balloon from the balloon object pools
			if (createInterval == 0) {
				Balloon balloon = balloonPool.newObject();
				balloon.y = 480;
				balloon.isFly = true;
				balloon.x = random.nextInt(690);
				balloon.color = random.nextInt(5);
				// If a new object was created add it to the list
				if (!balloonList.contains(balloon)) {
					balloonList.add(balloon);
				}
				
				createInterval = 30;
			} else {
				createInterval--;
			}
			
			for (Balloon item: balloonList) {
				if (item.y <= -BALLOON_HEIGHT) {
					balloonPool.free(item);
					item.y = 490;
					item.isFly = false;
				} else {
					item.fly();
				}
			}
		}
	}
	
	public void getImageXY(int balloon_x, int balloon_y) {
		image = Assets.images[random.nextInt(10)];
		if (balloon_x > 690 - image.getWidth()){
			image_x = 690 - image.getWidth();
		} else {
			image_x = balloon_x;
		}
		if (balloon_y > 480 - image.getHeight()) {
			image_y = 480 - image.getHeight();
		} else {
			image_y = balloon_y;
		}
	}
	public String calTimeLeft() {
		int minute = timeLeft < 0 ? 0 : timeLeft / 600;
		int second = timeLeft < 0 ? 0 : (timeLeft % 600)/10;
		String minute_s = "" + minute;
		String second_s = second < 10 ? "0" + second : "" + second;
		String time = minute_s + ":" + second_s;
		return time;
	}
}
