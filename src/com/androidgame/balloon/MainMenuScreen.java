package com.androidgame.balloon;

import java.util.List;

import com.androidgame.framework.Game;
import com.androidgame.framework.Graphic;
import com.androidgame.framework.Input.TouchEvent;
import com.androidgame.framework.Screen;

public class MainMenuScreen extends Screen{
	public MainMenuScreen(Game game) {
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		// Load graphics and input events
//		Graphic g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		game.getInput().getKeyEvents();
		// Temporarily let sound enabled
		Settings.soundEnabled = true;
		
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				// TODO Handling touch event to enable/disable sound
//				if (inBounds(event, 0, g.getHeight() - 64, 64, 64)) {
//					Settings.soundEnabled = !Settings.soundEnabled;
//					if (Settings.soundEnabled) {
//						Assets.click.play(1);
//					}
//					return;
//				}
				
				if (inBounds(event, 500, 160, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new GameScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
				
				if (inBounds(event, 500, 160+Assets.butHeight+30, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new HelpScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
				
				if (inBounds(event, 500, 160+(Assets.butHeight+30)*2, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new HighscoreScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
			}
		}
	}

	@Override
	public void present(float deltaTime) {
		// Draw the main menu interface to the virtual framebuffer
		Graphic g = game.getGraphics();
		
		g.drawPixmap(Assets.startBg, 0, 0);
		g.drawPixmap(Assets.startBt, 500, 160);
		g.drawPixmap(Assets.helpBt, 500, 160+Assets.butHeight+30);
		g.drawPixmap(Assets.highscBt, 500, 160+(Assets.butHeight+30)*2);
		
		// TODO Draw the enable/disable sound button
	}

	@Override
	public void pause() {
		Settings.save(game.getFileIO());
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}
}
