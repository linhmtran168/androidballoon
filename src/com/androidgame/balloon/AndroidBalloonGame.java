package com.androidgame.balloon;

import com.androidgame.framework.Screen;
import com.androidgame.framework.ngin.AndroidGame;

public class AndroidBalloonGame extends AndroidGame {
    /** Called when the activity is first created. */
	public Screen getStartScreen() {
		return new LoadingScreen(this);
	}
}