package com.androidgame.balloon;

import com.androidgame.framework.Game;
import com.androidgame.framework.Graphic;
import com.androidgame.framework.Graphic.PixmapFormat;
import com.androidgame.framework.Pixmap;
import com.androidgame.framework.Screen;

public class LoadingScreen extends Screen {
	public LoadingScreen(Game game){
		super(game);
	}
	
	@Override
	public void update(float deltaTime) {
		Graphic g = game.getGraphics();
		// Load game graphic and sound files
		Assets.gameBg = g.newPixmap("game.png", PixmapFormat.ARGB8888);
		Assets.helpBg = g.newPixmap("help.png", PixmapFormat.ARGB8888);
		Assets.hscoreBg = g.newPixmap("highscore.png", PixmapFormat.ARGB8888);
		Assets.endBg = g.newPixmap("notify.png", PixmapFormat.ARGB8888);
		Assets.resumeBg = g.newPixmap("resume.png", PixmapFormat.ARGB8888);
		Assets.startBg = g.newPixmap("start.png", PixmapFormat.ARGB8888);
		Assets.helpBt = g.newPixmap("help_but.png", PixmapFormat.ARGB8888);
		Assets.startBt = g.newPixmap("start_but.png", PixmapFormat.ARGB8888);
		Assets.highscBt = g.newPixmap("highscore_but.png", PixmapFormat.ARGB8888);
		Assets.mainBt = g.newPixmap("main_but.png", PixmapFormat.ARGB8888);
		Assets.contBt = g.newPixmap("continue_but.png", PixmapFormat.ARGB8888);
		Assets.viewAllBt = g.newPixmap("viewall_but.png", PixmapFormat.ARGB8888);
		Assets.redBl = g.newPixmap("red.png", PixmapFormat.ARGB8888);
		Assets.blueBl = g.newPixmap("blue.png", PixmapFormat.ARGB8888);
		Assets.violetBl = g.newPixmap("violet.png", PixmapFormat.ARGB8888);
		Assets.greenBl = g.newPixmap("green.png", PixmapFormat.ARGB8888);
		Assets.yellowBl = g.newPixmap("yellow.png", PixmapFormat.ARGB8888);
		
		Assets.butHeight = Assets.startBt.getHeight();
		Assets.butWidth = Assets.startBt.getWidth();
		
		Assets.clockIc = g.newPixmap("clock.png", PixmapFormat.ARGB8888);
		Assets.pauseIc = g.newPixmap("pause.png", PixmapFormat.ARGB8888);
		Assets.enSoundIc = g.newPixmap("enable.png", PixmapFormat.ARGB8888);
		Assets.disSoundIc = g.newPixmap("disable.png", PixmapFormat.ARGB8888);
		
		Assets.images = new Pixmap[10];
		for (int i = 0; i < 10; i++) {
			Assets.images[i] = g.newPixmap("img"+i+".png", PixmapFormat.ARGB8888);
		}
		
		Assets.click = game.getAudio().newSound("click.ogg");
		Assets.image = game.getAudio().newSound("image.ogg");
		Assets.balloon = game.getAudio().newSound("balloon.ogg");
		// Transition to the Main menu Screen
		
		// Load score file
		Settings.load(game.getFileIO());
		game.setScreen(new MainMenuScreen(game));
	}

	@Override
	public void present(float deltaTime) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}
}
