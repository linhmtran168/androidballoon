package com.androidgame.balloon;

import java.util.List;

import com.androidgame.framework.Game;
import com.androidgame.framework.Graphic;
import com.androidgame.framework.Input.TouchEvent;
import com.androidgame.framework.Screen;

public class GameOverScreen extends Screen {
	
	public GameOverScreen(Game game) {
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		// Load graphic, audio files and touch events
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 230, 120, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new GameScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
			}
			
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 450, 120, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new MainMenuScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
			}
		}
	}
	
	@Override
	public void present(float deltaTime) {
		// Draw the help interface
		Graphic g = game.getGraphics();
		
		String text = "Bạn đã bắt được " + Settings.lastScore + " khoảnh khắc của Hà Nội";
		g.drawPixmap(Assets.endBg, 0, 0);
		g.drawPixmap(Assets.contBt, 230, 120);
		g.drawPixmap(Assets.mainBt, 450, 120);
		g.drawGraphicText(text, 200, 90, 25, 0, true, "", "red");
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}
