package com.androidgame.balloon;

import com.androidgame.framework.Pixmap;
import com.androidgame.framework.Sound;

public class Assets {
	private Assets() {
		
	}
	
	public static Pixmap gameBg;
	public static Pixmap startBg;
	public static Pixmap endBg;
	public static Pixmap helpBg;
	public static Pixmap hscoreBg;
	public static Pixmap resumeBg;
	public static Pixmap mainBt;
	public static Pixmap helpBt;
	public static Pixmap contBt;
	public static Pixmap highscBt;
	public static Pixmap startBt;
	public static Pixmap viewAllBt;
	public static Pixmap redBl;
	public static Pixmap violetBl;
	public static Pixmap greenBl;
	public static Pixmap yellowBl;
	public static Pixmap blueBl;
	
	public static Pixmap clockIc;
	public static Pixmap pauseIc;
	public static Pixmap enSoundIc;
	public static Pixmap disSoundIc;
	
	public static Pixmap[] images;
	
	public static Sound click;
	public static Sound balloon;
	public static Sound image;
	
	public static int butHeight;
	public static int butWidth;
}
