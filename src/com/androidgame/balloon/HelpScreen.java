package com.androidgame.balloon;

import java.util.List;

import com.androidgame.framework.Game;
import com.androidgame.framework.Graphic;
import com.androidgame.framework.Input.TouchEvent;
import com.androidgame.framework.Screen;

public class HelpScreen extends Screen {
	public HelpScreen(Game game) {
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		// Load graphic, audio files and touch events
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 290, 410, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new MainMenuScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
			}
		}
	}
	
	@Override
	public void present(float deltaTime) {
		// Draw the help interface
		Graphic g = game.getGraphics();
		
		g.drawPixmap(Assets.helpBg, 0, 0);
		g.drawPixmap(Assets.mainBt, 290, 410);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}
