package com.androidgame.balloon;

public class Balloon {
	public final static int RED = 0;
	public final static int BLUE = 1;
	public final static int VIOLET = 2;
	public final static int GREEN = 3;
	public final static int YELLOW = 4;
	// Coordinate of the top left corner of the balloon 
	int x, y;
	int color;
	boolean isFly;
	float velocity = 2;
	
	public Balloon() {
		// TODO Auto-generated constructor stub
	}

	public void fly() {
		if (isFly) {
			y -= velocity;
		}
	}
}
