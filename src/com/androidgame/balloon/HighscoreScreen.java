package com.androidgame.balloon;

import java.util.List;

import com.androidgame.framework.Game;
import com.androidgame.framework.Graphic;
import com.androidgame.framework.Input.TouchEvent;
import com.androidgame.framework.Screen;

public class HighscoreScreen extends Screen {
	
	String[] lines = new String[10];
	
	public HighscoreScreen(Game game) {
		super(game);
		
		for (int i = 0; i < Settings.highscores.length; i++) {
			lines[i] = "" + (i + 1) + ". " + "You:                " + Settings.highscores[i];
		}
	}

	@Override
	public void update(float deltaTime) {
		// Load graphic, audio files and touch events
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 80, 400, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new MainMenuScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
				
				// TODO Implement view all users score
				if (inBounds(event, 580, 400, Assets.butWidth, Assets.butHeight)) {
//					game.setScreen(new ViewAllScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
			}
		}
	}
	
	@Override
	public void present(float deltaTime) {
		// Draw the help interface
		Graphic g = game.getGraphics();
		
		g.drawPixmap(Assets.hscoreBg, 0, 0);
		g.drawPixmap(Assets.mainBt, 80, 400);
		g.drawPixmap(Assets.viewAllBt,580, 400);
		drawScore(g);;
	}
	
	private void drawScore(Graphic g) {
		for (int i = 0; i < lines.length; i++) {
			g.drawGraphicText(lines[i], 260, 160+i*24, 26, 0, true, "", "black");
		}
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}
}
