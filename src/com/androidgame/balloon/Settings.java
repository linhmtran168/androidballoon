package com.androidgame.balloon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.androidgame.framework.FileIO;

public class Settings {
	private Settings() {
		
	}
	
	public static boolean soundEnabled = true;
	public static int[] highscores = new int[] { 100, 80, 50, 30, 10, 16, 17, 34, 45, 59 };
	public static int lastScore;
	
	public static void load(FileIO files) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(files.readFile(".balloon")));
//			soundEnabled = Boolean.parseBoolean(in.readLine());
			for (int i = 0; i < 10; i++) {
				highscores[i] = Integer.parseInt(in.readLine());
			}
		} catch (IOException e) {
			// Default
		} catch (NumberFormatException e) {
			// Default
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				// Default
			}
		}
	}
	
	public static void save(FileIO files) {
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(files.writeFile(".mrnom")));
//			out.write(Boolean.toString(soundEnabled));
			for (int i = 0; i < 10; i++) {
				out.write(Integer.toString(highscores[i]));
				}
		} catch (IOException e) {
			// Default
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				// Default
			}
		}
	}
	
	public static void addScore(int score) {
		for (int i = 0; i < 5; i++) {
			if (highscores[i] < score) {
				for (int j = 4; j > i; j--) {
					highscores[j] = highscores[j - 1];
				}
				highscores[i] = score;
				break;
			}
		}
	}
}
