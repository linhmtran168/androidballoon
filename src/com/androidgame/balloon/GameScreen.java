package com.androidgame.balloon;

import java.util.List;

import com.androidgame.framework.Game;
import com.androidgame.framework.Graphic;
import com.androidgame.framework.Input.TouchEvent;
import com.androidgame.framework.Pixmap;
import com.androidgame.framework.Screen;

public class GameScreen extends Screen {
	enum GameState {
		Ready,
		Running,
		Moment,
		Paused,
		Over
	}
	
	GameState state = GameState.Ready;
	int oldScore;
	String score = "0";
	long timeStart;
	float timeMoment;
	Stage stage;
	
	public GameScreen(Game game) {
		super(game);
		this.stage = new Stage();
	}

	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		game.getInput().getKeyEvents();
		
		if (state == GameState.Ready) {
			updateReady(touchEvents);
		}
		
		if (state == GameState.Running) {
			updateRunning(touchEvents, deltaTime);
		}
		
		if (state == GameState.Moment) {
			updateMoment(touchEvents);
		}
		
		if (state == GameState.Paused) {
			updatePaused(touchEvents);
		}
		
		if (state == GameState.Over) {
			game.setScreen(new GameOverScreen(game));
		}
	}

	private void updateReady(List<TouchEvent> touchEvents) {
		// Touch the screen to start play
		for (TouchEvent event: touchEvents) {
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 266, 176, Assets.butWidth, Assets.butHeight)) {
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					state = GameState.Running;
					timeStart = System.nanoTime();
					return;
				}
			}
			
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 266, 266, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new MainMenuScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
			}
		}
	}
	

	private void updatePaused(List<TouchEvent> touchEvents) {
		for (TouchEvent event: touchEvents) {
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 266, 176, Assets.butWidth, Assets.butHeight)) {
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					state = GameState.Running;
					return;
				}
			}
			
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 266, 266, Assets.butWidth, Assets.butHeight)) {
					game.setScreen(new MainMenuScreen(game));
					if (Settings.soundEnabled) {
						Assets.click.play(1);
					}
					return;
				}
			}
		}
	}

	private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
		for (TouchEvent event: touchEvents) {
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 745, 210, Assets.enSoundIc.getWidth(), Assets.enSoundIc.getHeight())) {
					Settings.soundEnabled = !Settings.soundEnabled;
				}
				
				if (inBounds(event, 745, 340, Assets.pauseIc.getWidth(), Assets.pauseIc.getHeight())) {
					state = GameState.Paused;
					return;
				}
			}
			
			if (event.type == TouchEvent.TOUCH_DOWN) {
				// Check if the finger touch the balloon
				touchMoment(event);
			}
			
		}
		
		stage.update(deltaTime, timeStart);
		if (stage.gameOver) {
			state = GameState.Over;
		}
		
		if (oldScore != stage.score) {
			oldScore = stage.score;
			score = "" + oldScore;
		}
	}
	
	private void updateMoment(List<TouchEvent> touchEvents) {
		if ((System.nanoTime() - timeMoment)/1000000000.0f >= 1) {
			state = GameState.Running;
			return;
		}		
	}

	
	public void touchMoment(TouchEvent touchEvent) {
		for (Balloon item: stage.balloonList) {
			if (inBounds(touchEvent, item.x, item.y, Stage.BALLOON_WIDTH, Stage.BALLOON_HEIGHT) && item.color == 0) {
				stage.score++;
				// Use the red balloon's x,y to calculate the appearing image's x,y
				stage.getImageXY(item.x, item.y);
				item.y = 490;
				if (Settings.soundEnabled) {
					Assets.image.play(1);
				}
				timeMoment = System.nanoTime();
				// Set the state to display the image
				state = GameState.Moment;
				return;
			}
			
			if (inBounds(touchEvent, item.x, item.y, Stage.BALLOON_WIDTH, Stage.BALLOON_HEIGHT) && item.color != 0) {
				item.y = 490;
				if (Settings.soundEnabled) {
					Assets.balloon.play(1);
				}
				return;
			}
		}
	}


	@Override
	public void present(float deltaTime) {
		Graphic g = game.getGraphics();
		
		g.drawPixmap(Assets.gameBg, 0, 0);
		g.drawPixmap(Assets.clockIc, 745, 20);
		g.drawGraphicText(stage.calTimeLeft(), 738, 80, 23, 0, true, "", "white");
		g.drawGraphicText("Score", 735, 120, 23, 0, true, "", "red");
		g.drawGraphicText(score, 760, 150, 23, 1, true, "", "white");
		
		if (Settings.soundEnabled) {
			g.drawPixmap(Assets.enSoundIc, 745, 210);
		} else {
			g.drawPixmap(Assets.disSoundIc, 745, 210);
		}
		
		g.drawPixmap(Assets.pauseIc, 745, 340);
		
		drawStage();
		
		if (state == GameState.Ready) {
			drawReadyUI();
		}
		if (state == GameState.Moment) {
			drawImage();
		}
		if (state == GameState.Paused) {
			drawPausedUI();
		}
	}
	
	private void drawImage() {
		Graphic g = game.getGraphics();
		
		// Draw the image after touch a red balloon
		g.drawPixmap(Stage.image, Stage.image_x, Stage.image_y);
	}

	private void drawReadyUI() {
		Graphic g = game.getGraphics();
		
		g.drawPixmap(Assets.resumeBg, 220, 140);
		g.drawPixmap(Assets.startBt, 266, 176);
		g.drawPixmap(Assets.mainBt, 266, 266);
	}
	
	private void drawStage() {
		Graphic g = game.getGraphics();
		// Load and draw the balloon to the view
		for (Balloon balloon: stage.balloonList) {
			Pixmap balPixmap = null;
			switch(balloon.color) {
			case 0:
				balPixmap = Assets.redBl;
				break;
			case 1:
				balPixmap = Assets.blueBl;
				break;
			case 2:
				balPixmap = Assets.violetBl;
				break;
			case 3:
				balPixmap = Assets.greenBl;
				break;
			case 4:
				balPixmap = Assets.yellowBl;
				break;
			}
			
			g.drawPixmap(balPixmap, balloon.x, balloon.y);
		}
	}
	
	private void drawPausedUI() {
		Graphic g = game.getGraphics();
		
		g.drawPixmap(Assets.resumeBg, 220, 140);
		g.drawPixmap(Assets.contBt, 266, 176);
		g.drawPixmap(Assets.mainBt, 266, 266);
	}
	

	@Override
	public void pause() {
		if (state == GameState.Running) {
			state = GameState.Paused;
		}
		
		if (stage.gameOver) {
			Settings.addScore(stage.score);
			Settings.save(game.getFileIO());
			Settings.lastScore = stage.score;
		}
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}
}
